$(document).ready(function(){
    $("#btnLoginModal").click(function(){
        $(".nav__modal").addClass("active");
        $(".nav__form").addClass("animateZoom");
        $(".nav-tabs li:first-child").addClass("active");
        $(".nav-tabs li:last-child").removeClass("active");
        $(".nav__form--login").addClass("in active");
        $(".nav__form--register").removeClass("in active");
    });
    $("#btnRegisterModal").click(function(){
        $(".nav__modal").addClass("active");
        $(".nav__form").addClass("animateZoom");
        $(".nav-tabs li:first-child").removeClass("active");
        $(".nav-tabs li:last-child").addClass("active");
        $(".nav__form--login").removeClass("in active");
        $(".nav__form--register").addClass("in active");
    });
    $("#modalClose").click(function(){
        $(".nav__modal").removeClass("active");
        $(".nav__form").removeClass("animateZoom");
    });
    $("#btnLogOut").click(function(){
        $(".form").css("display", "block");
        $(".member").css("display", "none");
    });  
    $("#btnLogIn").click(function(){
        $(".form").css("display", "none");
        $(".member").css("display", "block");
    });      
});