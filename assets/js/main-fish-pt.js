$('.button_blue').click(function() {
    $('.helpoverlay').addClass('active');
});

$('.helpoverlay_close').click(function() {
    $('.helpoverlay').removeClass('active');
});

// open help page for Cash Fishing
function openHelp(url) {
    if (typeof window.chrome != "object") {
        newwindow = window.open(url, 'name', 'height=770,width=1010,scrollbars=yes,resizable=yes');
        if (window.focus) {
            newwindow.focus()
        }
        return false;
    } else {
        newwindow = window.open(url, 'name', 'height=770,width=1010,scrollbars=yes,resizable=yes');
        if (window.focus) {
            newwindow.focus()
        }
        return false;
    }
}

$('.button').click(function() {
    $('.button').removeClass('active');
    $(this).addClass('active');
    var bID = $(this).attr('id')
    $('.content-pic').removeClass('active');
    $("." + bID).addClass('active');
});