     
(function() {

  "use strict";

  var toggles = document.querySelectorAll(".menu__icon");

  for (var i = toggles.length - 1; i >= 0; i--) {
    var toggle = toggles[i];
    toggleHandler(toggle);
  };

  function toggleHandler(toggle) {
    toggle.addEventListener( "click", function(e) {
      e.preventDefault();
      (this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
    });
  }

})();


$(document).ready(function(){
  $(".menu__icon-burger").click(function(){
      $(".wrap").toggleClass("wrap--mobile");
      $(".head_bottom").toggleClass("nav__mobile");
  });
  $(".main__nav_dropdown--slots").click(function(event){
      $(".main__nav_dropdown").toggleClass("nav--active-mobile");
      $(".caret").toggleClass("caret-active");
      event.preventDefault();
  });
  $(".main_menu").hover(function(){
    $(this).toggleClass('nav--active-mobile');
    $(this).find('.menu_item').toggleClass('nav--active-mobile');
  });
  $(".main_menu").click(function(){
    $(this).toggleClass('nav--active-mobile');
    $(this).find('.menu_item').toggleClass('nav--active-mobile');
  });
});
