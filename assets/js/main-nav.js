$(document).ready(function(){
	$('.main__nav li a').hover(function() {
	    $(this).toggleClass('nav--active');
	    $(this).find('.menu_item').toggleClass('nav--active');
	});
	$('.main__nav_dropdown--slots').hover(function(){
		$('.main__nav_dropdown').addClass('menu--active');
	},function(){
		$('.main__nav_dropdown').hover(function(){}, function(){
			$('.main__nav_dropdown').removeClass('menu--active');
		});
	});	
	$('.header_bg, .fp-slide').bind('contextmenu', function(e) {
	    return false;
	});
});