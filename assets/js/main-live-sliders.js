$('.live__slider').slick({
  slidesToShow: 5,
  slidesToScroll: 3,
  autoplay: true,
  autoplaySpeed: 2000,
  infinite: true,
  dots: false,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 3,
      }
    },
    {
      breakpoint: 700,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

/* live casino play button visit section */
$(document).on('click', '#live_sa_gaming', function(){
  $.fn.fullpage.moveTo(4);
});

$(document).on('click', '#live_mg_gaming', function(){
  $.fn.fullpage.moveTo(5);
});

$(document).on('click', '#live_ag_gaming', function(){
  $.fn.fullpage.moveTo(6);
});

$(document).on('click', '#live_pt_gaming', function(){
  $.fn.fullpage.moveTo(7);
});

$(document).on('click', '.backto_live', function(){
  $.fn.fullpage.moveTo(3);
});