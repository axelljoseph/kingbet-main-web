$(document).ready(function(){
    $(".slot__img--pt").click(function(){
        $(".slot__banner").remove().hide().fadeOut(500);
        $(".slot__header").hide().fadeIn(500).append('<img src="assets/images/slots/headers/PT.jpg" class="slot__banner">');
        $(".slot__games--header-title").addClass("slot__games--header-pt");
        $(".slot__games--header-title").removeClass("slot__games--header-mg");
        $(".slot__games--header-title").removeClass("slot__games--header-ttg");
        $(".slot__games--header-title").removeClass("slot__games--header-png");
        $(".slot__games--header-title").removeClass("slot__games--header-bs");
        $(".slot__games--header-title").removeClass("slot__games--header-isb");
        $(".slot__games--header-title").removeClass("slot__games--header-fg");
        $(".slot__games--header-title").removeClass("slot__games--header-hb");
        $(".slot__img--platform").removeClass("slot__active");
        $(this).addClass("slot__active");
    });
    $(".slot__img--mg").click(function(){
        $(".slot__banner").remove().hide().fadeOut(500);
        $(".slot__header").hide().fadeIn(500).append('<img src="assets/images/slots/headers/MG.jpg" class="slot__banner">');
        $(".slot__games--header-title").addClass("slot__games--header-mg");
        $(".slot__games--header-title").removeClass("slot__games--header-pt");
        $(".slot__games--header-title").removeClass("slot__games--header-ttg");
        $(".slot__games--header-title").removeClass("slot__games--header-png");
        $(".slot__games--header-title").removeClass("slot__games--header-bs");
        $(".slot__games--header-title").removeClass("slot__games--header-isb");
        $(".slot__games--header-title").removeClass("slot__games--header-fg");
        $(".slot__games--header-title").removeClass("slot__games--header-hb");
        $(".slot__img--platform").removeClass("slot__active");
        $(this).addClass("slot__active");
        $(".slot__games--header-pt").fadeIn(500)
    });
    $(".slot__img--bs").click(function(){
        $(".slot__banner").remove().hide().fadeOut(500);
        $(".slot__header").hide().fadeIn(500).append('<img src="assets/images/slots/headers/BS.jpg" class="slot__banner">');
        $(".slot__games--header-title").addClass("slot__games--header-bs");
        $(".slot__games--header-title").removeClass("slot__games--header-mg");
        $(".slot__games--header-title").removeClass("slot__games--header-ttg");
        $(".slot__games--header-title").removeClass("slot__games--header-png");
        $(".slot__games--header-title").removeClass("slot__games--header-pt");
        $(".slot__games--header-title").removeClass("slot__games--header-isb");
        $(".slot__games--header-title").removeClass("slot__games--header-fg");
        $(".slot__games--header-title").removeClass("slot__games--header-hb");
        $(".slot__img span img").removeClass("slot__active");
        $(this).addClass("slot__active");
    });
    $(".slot__img--ttg").click(function(){
        $(".slot__banner").remove().hide().fadeOut(500);
        $(".slot__header").hide().fadeIn(500).append('<img src="assets/images/slots/headers/TTG.jpeg" class="slot__banner">');
        $(".slot__games--header-title").addClass("slot__games--header-ttg");
        $(".slot__games--header-title").removeClass("slot__games--header-mg");
        $(".slot__games--header-title").removeClass("slot__games--header-pt");
        $(".slot__games--header-title").removeClass("slot__games--header-png");
        $(".slot__games--header-title").removeClass("slot__games--header-bs");
        $(".slot__games--header-title").removeClass("slot__games--header-isb");
        $(".slot__games--header-title").removeClass("slot__games--header-fg");
        $(".slot__games--header-title").removeClass("slot__games--header-hb");
        $(".slot__img span img").removeClass("slot__active");
        $(this).addClass("slot__active");
    });
    $(".slot__img--png").click(function(){
        $(".slot__banner").remove().hide().fadeOut(500);
        $(".slot__header").hide().fadeIn(500).append('<img src="assets/images/slots/headers/PNG.jpeg" class="slot__banner">');
        $(".slot__games--header-title").addClass("slot__games--header-png");
        $(".slot__games--header-title").removeClass("slot__games--header-mg");
        $(".slot__games--header-title").removeClass("slot__games--header-ttg");
        $(".slot__games--header-title").removeClass("slot__games--header-pt");
        $(".slot__games--header-title").removeClass("slot__games--header-bs");
        $(".slot__games--header-title").removeClass("slot__games--header-isb");
        $(".slot__games--header-title").removeClass("slot__games--header-fg");
        $(".slot__games--header-title").removeClass("slot__games--header-hb");
        $(".slot__img span img").removeClass("slot__active");
        $(this).addClass("slot__active");
    });
    $(".slot__img--isb").click(function(){
        $(".slot__banner").remove().hide().fadeOut(500);
        $(".slot__header").hide().fadeIn(500).append('<img src="assets/images/slots/headers/ISB.jpg" class="slot__banner">');
        $(".slot__games--header-title").addClass("slot__games--header-isb");
        $(".slot__games--header-title").removeClass("slot__games--header-mg");
        $(".slot__games--header-title").removeClass("slot__games--header-ttg");
        $(".slot__games--header-title").removeClass("slot__games--header-png");
        $(".slot__games--header-title").removeClass("slot__games--header-bs");
        $(".slot__games--header-title").removeClass("slot__games--header-pt");
        $(".slot__games--header-title").removeClass("slot__games--header-fg");
        $(".slot__games--header-title").removeClass("slot__games--header-hb");
        $(".slot__img span img").removeClass("slot__active");
        $(this).addClass("slot__active");
    });
    $(".slot__img--fg").click(function(){
        $(".slot__banner").remove().hide().fadeOut(500);
        $(".slot__header").hide().fadeIn(500).append('<img src="assets/images/slots/headers/FG.jpg" class="slot__banner">');
        $(".slot__games--header-title").addClass("slot__games--header-fg");
        $(".slot__games--header-title").removeClass("slot__games--header-mg");
        $(".slot__games--header-title").removeClass("slot__games--header-ttg");
        $(".slot__games--header-title").removeClass("slot__games--header-png");
        $(".slot__games--header-title").removeClass("slot__games--header-bs");
        $(".slot__games--header-title").removeClass("slot__games--header-isb");
        $(".slot__games--header-title").removeClass("slot__games--header-pt");
        $(".slot__games--header-title").removeClass("slot__games--header-hb");
        $(".slot__img span img").removeClass("slot__active");
        $(this).addClass("slot__active");
    });
    $(".slot__img--hb").click(function(){
        $(".slot__banner").remove().hide().fadeOut(500);
        $(".slot__header").hide().fadeIn(500).append('<img src="assets/images/slots/headers/HB.jpg" class="slot__banner">');
        $(".slot__games--header-title").addClass("slot__games--header-hb");
        $(".slot__games--header-title").removeClass("slot__games--header-mg");
        $(".slot__games--header-title").removeClass("slot__games--header-ttg");
        $(".slot__games--header-title").removeClass("slot__games--header-png");
        $(".slot__games--header-title").removeClass("slot__games--header-bs");
        $(".slot__games--header-title").removeClass("slot__games--header-isb");
        $(".slot__games--header-title").removeClass("slot__games--header-fg");
        $(".slot__games--header-title").removeClass("slot__games--header-pt");
        $(".slot__img span img").removeClass("slot__active");
        $(this).addClass("slot__active");
    });
    $(".slot__games--category-game ul li").click(function(){
        event.preventDefault();
        $(".slot__games--category-game ul li:first-child").removeClass("slot__games--category-active");
        $(this).toggleClass("slot__games--category-active");
    });
    $(".slot__games--category-game ul li:first-child").click(function(){
        event.preventDefault();
        $(".slot__games--category-game ul li").removeClass("slot__games--category-active");
        $(this).toggleClass("slot__games--category-active");
    });

    $(".slot__games--category-money ul li").click(function(){
        event.preventDefault();
        $(".slot__games--category-money ul li:first-child").removeClass("slot__games--category-active");
        $(this).toggleClass("slot__games--category-active");
    });
    $(".slot__games--category-money ul li:first-child").click(function(){
        event.preventDefault();
        $(".slot__games--category-money ul li").removeClass("slot__games--category-active");
        $(this).toggleClass("slot__games--category-active");
    });
});
