$(document).ready(function(){
    // $(".app__head").css('opacity', 0).slideDown(1000).animate({opacity: 1},{queue: false, duration: 2000});

    $(".promo__controls--list").click(function(){
        $(".promo--slider").fadeOut();
        $(".promo--slider").addClass("promo--inactive");
        $(".promo--list").fadeIn(1500);
        $(".promo--list").removeClass("promo--inactive");
    });
    $(".promo__controls--slider").click(function(){
        $(".promo--list").fadeOut();
        $(".promo--list").addClass("promo--inactive");
        $(".promo--slider").fadeIn(1500);
        $(".promo--slider").removeClass("promo--inactive");
    });
    $(".promo__heading").click(function(){
        $(this).siblings(".promo__content").slideToggle("slow");
        event.preventDefault()
    });
    // $(".buttonNav").click(function(){
    //     $(this).find(".promo__content").slideToggle("slow", function(){
    //         $(".carousel").addClass("addHeight");
    //     });
    // });
    $("#showSlider").click(function(){
        $(".promo--list").addClass("promo--inactive");
        $(".promo--slider").removeClass("promo--inactive");

    });
    $(".heading--gift").hide().fadeIn(1000);
    $(".promo").hide().fadeIn(2000);
});
